import React from 'react'
import logo from '../images/content/logo.png'

const Splash = () => {
  return (
    <section>
        <div className="container mx-auto min-h-screen">
          <div className="flex flex-col items-center h-screen justify-center text-center mb-4">
            <div className="w-full md:w-4/12 text-center">
              <img src={logo} alt="Logo" className="mx-auto mb-8" />
              <p className="mb-16 px-4">
                Kami menyediakan furniture berkelas yangmembuat ruangan terasa homey
              </p>
            </div>
          </div>
        </div>
    </section>
  )
}

export default Splash